import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt


def ik_sharp_bandwidth(df, x, y, cut=0):
    '''
    DESCRIPTION:
        Applies to Sharp RDD
        For a given outcome Y and running variable X, computes the optimal bandwidth
        h using a triangular kernel. For more information, see
        "OPTIMAL BANDWIDTH CHOICE FOR THE REGRESSION DISCONTINUITY ESTIMATOR",
        by Imbens and Kalyanaraman, at http://www.nber.org/papers/w14726.pdf
        Original Code from evan-magnusson https://github.com/evan-magnusson/rdd/blob/master/rdd/rdd.py
        Minor adjustments by Aloisio Dourado
    INPUTS:
        Two equal length pandas Series
            y: the name of the outcome variable
            x: the name of the running variable
        cut: value for the threshold of the rdd (scalar) (default is 0)

    OUTPUTS:
        Scalar optimal bandwidth value
    '''

    X, Y = df[x], df[y]

    # Normalize X
    X = X - cut

    # Step 1
    h1 = 1.84 * X.std() * (X.shape[0] ** (-.2))
    Nh1neg = X[(X < 0) & (X > -h1)].shape[0]
    Nh1pos = X[(X >= 0) & (X < h1)].shape[0]
    Ybarh1neg = Y[(X < 0) & (X > -h1)].mean()
    Ybarh1pos = Y[(X >= 0) & (X < h1)].mean()
    fXc = (Nh1neg + Nh1pos) / (2 * X.shape[0] * h1)
    sig2c = (((Y[(X < 0) & (X > -h1)] - Ybarh1neg) ** 2).sum() + ((Y[(X >= 0) & (X < h1)] - Ybarh1pos) ** 2).sum()) / (
                Nh1neg + Nh1pos)

    # Step 2
    medXneg = X[X < 0].median()
    medXpos = X[X >= 0].median()
    dat_temp = pd.DataFrame({'Y': Y, 'X': X})
    dat_temp = dat_temp.loc[(dat_temp['X'] >= medXneg) & (dat_temp['X'] <= medXpos)]
    dat_temp['treat'] = 0
    dat_temp.loc[dat_temp['X'] >= 0, 'treat'] = 1
    dat_temp['X2'] = X ** 2
    dat_temp['X3'] = X ** 3
    eqn = 'Y ~ 1 + treat + X + X2 + X3'
    results = smf.ols(eqn, data=dat_temp).fit()
    m3 = 6 * results.params.loc['X3']
    h2pos = 3.56 * (X[X >= 0].shape[0] ** (-1 / 7.0)) * (sig2c / (fXc * np.max([m3 ** 2, .01]))) ** (1 / 7.0)
    h2neg = 3.56 * (X[X < 0].shape[0] ** (-1 / 7.0)) * (sig2c / (fXc * np.max([m3 ** 2, .01]))) ** (1 / 7.0)
    Yplus = Y[(X >= 0) & (X <= h2pos)]
    Xplus = X[(X >= 0) & (X <= h2pos)]
    dat_temp = pd.DataFrame({'Y': Yplus, 'X': Xplus})
    dat_temp['X2'] = X ** 2
    eqn = 'Y ~ 1 + X + X2'
    results = smf.ols(eqn, data=dat_temp).fit()
    m2pos = 2 * results.params.loc['X2']
    Yneg = Y[(X < 0) & (X >= -h2neg)]
    Xneg = X[(X < 0) & (X >= -h2neg)]
    dat_temp = pd.DataFrame({'Y': Yneg, 'X': Xneg})
    dat_temp['X2'] = X ** 2
    eqn = 'Y ~ 1 + X + X2'
    results = smf.ols(eqn, data=dat_temp).fit()
    m2neg = 2 * results.params.loc['X2']

    # Step 3
    rpos = 720 * sig2c / (X[(X >= 0) & (X <= h2pos)].shape[0] * h2pos ** 4)
    rneg = 720 * sig2c / (X[(X < 0) & (X >= -h2neg)].shape[0] * h2neg ** 4)
    CK = 3.4375
    hopt = CK * (2 * sig2c / (fXc * ((m2pos - m2neg) ** 2 + (rpos + rneg)))) ** .2 * Y.shape[0] ** (-.2)

    return hopt


def ik_fuzzy_bandwidth(df, x, y, w, cut=0):
    '''
    DESCRIPTION:
        Applies to Fuzzy RDD
        For a given outcome Y and running variable X, computes the optimal bandwidth
        h using a triangular kernel. For more information, see
        "OPTIMAL BANDWIDTH CHOICE FOR THE REGRESSION DISCONTINUITY ESTIMATOR",
        by Imbens and Kalyanaraman, at http://www.nber.org/papers/w14726.pdf
        Original Code from evan-magnusson https://github.com/evan-magnusson/rdd/blob/master/rdd/rdd.py
        Minor adjustments by Aloisio Dourado
    INPUTS:
        Two equal length pandas Series
            y: the name of the outcome variable
            x: the name of the running variable
        cut: value for the threshold of the rdd (scalar) (default is 0)

    OUTPUTS:
        Scalar optimal bandwidth value
    '''

    hopt_y = ik_sharp_bandwidth(df, x, y, cut=cut)
    hopt_w = ik_sharp_bandwidth(df, x, w, cut=cut)
    return {"hopt_y": hopt_y, "hopt_w": hopt_w}


def bin_data(data, yname, xname, bins=50, agg_fn=np.mean):
    '''
    When datasets are so large that traditional RDD scatter plots are difficult to read,
        this will group observations by their X values into a set number of bins and compute
        the mean outcome value in that bin.
    Original Code from evan-magnusson https://github.com/evan-magnusson/rdd/blob/master/rdd/rdd.py

    INPUT:
        data: dataset (pandas DataFrame)
        yname: Name of outcome variable (string)
        xname: Name of running variable (string)
        bins: Desired number of bins to group data by (integer) (default is 50)
    OUTPUT:
        A pandas DataFrame that has a row for each bin with columns:
            yname: The average value of the outcome variable in that bin
            xname: the midpoint value of the running variable in that bin
            n_obs: The number of observations in this bin

    '''
    hist, edges = np.histogram(data[xname], bins=bins)
    bin_midpoint = np.zeros(edges.shape[0] - 1)
    binned_df = pd.DataFrame(np.zeros((edges.shape[0] - 1, 1)))
    for i in range(edges.shape[0] - 1):
        bin_midpoint[i] = (edges[i] + edges[i + 1]) / 2
        if i < edges.shape[0] - 2:
            dat_temp = data.loc[(data[xname] >= edges[i]) & (
                    data[xname] < edges[i + 1]), :]
            binned_df.loc[binned_df.index[i], yname] = agg_fn(dat_temp[yname])
            binned_df.loc[binned_df.index[i], xname] = bin_midpoint[i]
            binned_df.loc[binned_df.index[i], 'n_obs'] = dat_temp.shape[0]
        else:
            dat_temp = data.loc[(data[xname] >= edges[i]) & (
                    data[xname] <= edges[i + 1]), :]
            binned_df.loc[binned_df.index[i], yname] = agg_fn(dat_temp[yname])
            binned_df.loc[binned_df.index[i], xname] = bin_midpoint[i]
            binned_df.loc[binned_df.index[i], 'n_obs'] = dat_temp.shape[0]
    return binned_df


def truncated_data(data, xname, bandwidth=None, yname=None, cut=0):
    '''
    Drop observations from dataset that are outside
        a given (or optimal) bandwidth
    INPUTS:
        data: data with the X and Y values (pandas DataFrame)
        xname: Name of your running variable (string)
        bandwidth: Bandwidth (scalar) (if None given, the optimal bandwidth is computed)
        yname: The name of your outcome variable (string) (only needed if no bandwidth is given)
        cut: The value of your threshold (scalar) (default is 0)
    OUTPUTS:
        pandas DataFrame with observations outside of the bandwidth dropped

    '''
    if bandwidth == None:
        if yname == None:
            raise NameError("You must supply either a bandwidth or the name of your outcome variable.")
        else:
            bandwidth = optimal_bandwidth(data[yname], data[xname], cut=cut)
    data_new = data.loc[np.abs(data[xname] - cut) <= bandwidth,]
    return data_new


def triang_kernel(x, cut, h):
    indicator = (np.abs(x - cut) <= h).astype(float)
    return indicator * (1 - np.abs(x - cut) / h)


def sharp_rdd(input_data, xname, yname=None, cut=0, covars=None, bw=None, kernel="triang"):
    '''
    This function implements a linear regression (ordinary or weighted least squares can be used) for
        the estimation of regressing the outcome variable on the running variable.  A "TREATED" variable
        is created, the coefficient on which is the causal effect of being to the right of the threshold.
        The user may specify a list of controls to be added linearly, or supply their own equation.
    INPUT:
        input_data: dataset with outcome and running variables (and potentially controls) (pandas DataFrame)
        xname: name of running variable (string)
        yname: name of outcome variable (string) (default is None - not needed if you include your own equation)
        cut: location of threshold in xname (scalar) (default is 0)
        equation: Estimation equation as a string (see Statsmodels formula syntax for more info)
        controls: List of controls to include in the estimation (list of strings) (not needed if you include your own equation)
        noconst: If True, model does not estimate an intercept (bool) (default is false)
        weights: Weights for weighted least squares (numpy array) (default is equal weights, ie OLS)
    OUTPUT:
        Statsmodels object
    '''

    if (kernel is not None) and (kernel != "triang"):
        raise NameError("Kernel must be 'triang' or None")

    if (kernel is not None) and (bw is None):
        raise NameError("BandWidth (bw) is required for triangular kernel")

    if 'threshold' in input_data.columns:
        raise NameError("threshold is a reserved column name.  Please change the name.")

    rdd_df = input_data.copy() if bw is None else truncated_data(input_data.copy(), xname, bandwidth=bw, yname=yname,
                                                                 cut=cut)

    weights = 1 if kernel is None else triang_kernel(rdd_df[xname], cut=cut, h=bw)

    rdd_df[xname] -= cut

    rdd_df = rdd_df.assign(threshold=(rdd_df[xname] > 0).astype(int))

    equation = yname + '~' + xname + '*threshold'
    equation += '' if covars is None else ' + ' + ' + '.join(covars)
    print(equation)

    model = smf.wls(equation, rdd_df, weights=weights).fit()

    return model

def fuzzy_rdd(input_data, xname, yname, wname, cut=0, covars=None, bw_y=None, bw_w=None, kernel="triang"):

   model_y =  sharp_rdd(input_data=input_data, xname=xname, yname=yname, cut=cut,
                        covars=covars, bw=bw_y, kernel=kernel)

   model_w =  sharp_rdd(input_data=input_data, xname=xname, yname=wname, cut=cut,
                        covars=covars, bw=bw_w, kernel=kernel)

   impact = model_y.params["threshold"]/model_w.params["threshold"]

   return {"impact":impact, "model_y":model_y, "model_w":model_w}


def sim_data(samples, impact=.5, cut=1, x_dist=(1, 1),
             noise_dist=(0, 1), intercept=1, c1_dist=(0, 1), c2_dist=(0, 4),
             x_coef=2, c1_coef=.2, c2_coef=0, fuzy=False):
    np.random.seed(1)

    x = np.random.normal(x_dist[0], x_dist[1], samples)  # Variável de controle

    noise = 0.2 * np.random.normal(noise_dist[0], noise_dist[1], samples) * (abs(x - cut) + 1)

    if fuzy:
        w_prob = (1 + np.tanh(4 * (x - min(x)) / (max(x) - min(x)) - 2)) / 4 + .5 * (x >= cut)
        w_rand = abs(np.random.uniform(0, 1, samples))
        w = np.where(w_prob >= w_rand, 1, 0)
    else:
        w = np.where(x >= cut, 1, 0)  # Tratamento Sim=1 ou Não=0

    c1, c2 = np.random.normal(c1_dist[0], c1_dist[1], samples), np.random.normal(c2_dist[0], c2_dist[1],
                                                                                 samples)  # Covariáveis

    # Expressão do resultado
    y = (impact * w) + x_coef * x + c1_coef * c1 + c2_coef * c2 + intercept + noise

    data = pd.DataFrame({'y': y, 'x': x, 'c1': c1, 'c2': c2, 'w': w})

    return data


def density_test(data, xname, wname, n_bins=200):
    hist, edges = np.histogram(data[xname], bins=n_bins)
    w = data[wname]
    w_density, x_density, mid_point = np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins)
    h0 = 0
    for n, h in enumerate(hist):
        w_density[n] = np.mean(w[h0:h0 + h])
        mid_point[n] = (edges[n] + edges[n + 1]) / 2
        x_density[n] = h
        h0 += h
    return pd.DataFrame({'mid_point': mid_point, 'x_density': x_density, 'w_density': w_density})
